# API-Tareas

 Aplicación fue desarrollada con Spring Boot(api rest), Angular(front-end) y MongoDB

## Api rest

- Creada con lenguaje Java usando el framework Spring configurado con Maven

### Dependecias utilizadas

- Spring Web
- Spring Data MongoDB
- Lombok

## Base de datos

- Para persistir los datos se uso MongoDB de manera local

## Front End

- Para visualizar/listar, crear, modificar y eliminar las tareas se uso el framework React

### Dependencias instaladas

- *reactstrap - componentes*
  - npm install --save reactstrap react react-dom
  - sitio web: https://reactstrap.github.io/
- *Availity reactstrap Validation - validaciones*
  - npm install --save availity-reactstrap-validation react react-dom
  - sitio web: https://availity.github.io/availity-reactstrap-validation/
- *bootstrap - estilos*
  - npm install bootstrap --save
- *font awesome - fuentws*
  - npm i --save @fortawesome/fontawesome-svg-core
  - npm install --save @fortawesome/free-solid-svg-icons
  - npm install --save @fortawesome/react-fontawesome
- *axios - operacions http*
  - Axios es una librería JavaScript que puede ejecutarse en el navegador y que nos permite hacer sencillas las operaciones como cliente HTTP, por lo que podremos configurar y realizar solicitudes a un servidor y recibiremos respuestas fáciles de procesar
  - npm i axios --save


<br>

<p align="center">
  <img src="tareasclientereact/src/img/react.png" width="800" height="400"title="tareas">
</p>
