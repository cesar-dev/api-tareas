import React from 'react';
import { AvForm, AvField, AvInput, AvGroup } from 'availity-reactstrap-validation';
import { Button, Label } from 'reactstrap';
import { useRef } from 'react/cjs/react.development';

const FormularioTarea = ({ tarea, onSubmit }) => {

    //Resetear lo valores el formulario
    let form = useRef();

    const _onSubmit = (valores) => {
        onSubmit(valores);
        form.reset();
    }

    return (
        <>
            <div className="container mt-5">
                <h3 className="mb-3">{tarea ? 'Editar' :'Nueva'} tarea</h3>
                <AvForm ref={c => form = c} onValidSubmit={(_, values) => _onSubmit(values)}>
                    <AvGroup className="mb-3">
                        <AvField name="nombre" label="Nombre" required value={tarea ? tarea.nombre : ''} />
                    </AvGroup>
                    <AvGroup check className="mb-3">
                        <AvInput type="checkbox" name="completado" checked={tarea ? tarea.completado : false} />
                        <Label check for="completado">Completado</Label>
                    </AvGroup>

                    <div className="text-end">
                        <Button color="primary">Guardar</Button>
                    </div>
                </AvForm>
            </div>
        </>
    )
}


export default FormularioTarea;
