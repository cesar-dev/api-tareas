//import logo from './logo.svg';
import axios from 'axios';
import { useEffect, useState } from 'react/cjs/react.development';
import { Col, Container, Row } from 'reactstrap';
import './App.css';
import Banner from './Banner';
import FormularioTarea from './FormularioTarea';
import ListaTareas from './ListarTareas';


/* https://www.youtube.com/watch?v=1turU7imCzE&t=19s */
function App() {

    const [tareas, setTareas] = useState([]);
    const [tarea, setTarea] = useState();

    const cargarTareas = () => {
        axios.get('http://localhost:8080/tareas')
            .then(({ data }) => setTareas(data));
    }

    useEffect(cargarTareas, []);



    const onSubmit = (values) => {

        if (tarea) {
            axios.put(`http://localhost:8080/tareas/${tarea.id}`, values)
                .then(() => {
                    setTarea();
                    cargarTareas();
                });

        } else {
            axios.post('http://localhost:8080/tareas', values)
                .then(() => cargarTareas());
        }

    }

    const eliminarTarea = (tarea) => {
        axios.delete(`http://localhost:8080/tareas/${tarea.id}`)
            .then(() => cargarTareas());
    }



    return (
        <>
            <Container>
                <Banner/>


                <Row>
                    <Col md={6}>
                        <ListaTareas tareas={tareas} onDelete={eliminarTarea} onEdit={(tarea) => setTarea(tarea)} />
                    </Col>
                    <Col md={6}>
                        <FormularioTarea onSubmit={onSubmit} tarea={tarea} />
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default App;
