import React from 'react';
import img from './img/task.svg'

const Banner = () => {
    return (

        <div className="rounded shadow p-3 mt-4 d-flex">
            <img src={img} alt="" width="250"></img>
            <div>
                <h3 className="text-primary">Bienvenido a Tareas</h3>
                <h5 className="text-secondary">Con esta aplicación podras consultar, editar y eliminar tus tareas</h5>
                <hr />
                <p className="text-muted">
                    Esta aplicación fue desarrollada con Spring boot, React.js y mongoDB
                </p>
            </div>
        </div>
    );
}



export default Banner;
